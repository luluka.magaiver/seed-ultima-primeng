import { Injectable } from "@angular/core";
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router
} from "@angular/router";
import { Observable } from "rxjs";

import { LoginService } from "./demo/view/login/login.service";

@Injectable({
    providedIn: "root"
})
export class AuthGuard implements CanActivate {
    constructor(private loginService: LoginService, private router: Router) {}
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (this.loginService.isAccessTokenInvalido()) {
            console.log(
                "Navegação com access token inválido. Obtendo novo token..."
            );

            return this.loginService.obterNovoAccessToken().then(() => {
                if (this.loginService.isAccessTokenInvalido()) {
                    this.router.navigate(["/login"]);
                    return false;
                }

                return true;
            });
        } else if (
            next.data.roles &&
            !this.loginService.temQualquerPermissao(next.data.roles)
        ) {
            this.router.navigate(["/nao-autorizado"]);
            return false;
        }

        return true;
    }
}
