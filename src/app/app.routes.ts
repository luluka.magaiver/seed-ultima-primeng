import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { LoginComponent } from "./demo/view/login/login.component";
import { DashboardComponent } from "./demo/view/dashboard/dashboard.component";
import { NaoAutorizadoComponent } from "./demo/view/nao-autorizado/nao-autorizado.component";
import { UsuarioCadastroComponent } from "./demo/view/usuario/usuario-cadastro/usuario-cadastro.component";
import { UsuarioPesquisaComponent } from "./demo/view/usuario/usuario-pesquisa/usuario-pesquisa.component";
import { AuthGuard } from "./auth.guard";

export const routes: Routes = [
    {
        path: "login",
        component: LoginComponent
    },
    {
        path: "",
        component: DashboardComponent,
        canActivate: [AuthGuard],
        data: { roles: ["ROLE_DASHBOARD", "ROLE_ADMINISTRADOR"] }
    },
    {
        path: "usuario",
        component: UsuarioPesquisaComponent,
        canActivate: [AuthGuard],
        data: { roles: ["ROLE_VISUALIZAR_USUARIO", "ROLE_ADMINISTRADOR"] }
    },
    {
        path: "usuario/novo",
        component: UsuarioCadastroComponent,
        canActivate: [AuthGuard],
        data: { roles: ["ROLE_CRIAR_USUARIO", "ROLE_ADMINISTRADOR"] }
    },
    {
        path: "usuario/:id",
        component: UsuarioCadastroComponent,
        canActivate: [AuthGuard],
        data: { roles: ["ROLE_ALTERAR_USUARIO", "ROLE_ADMINISTRADOR"] }
    },
    {
        path: "nao-autorizado",
        component: NaoAutorizadoComponent,
        canActivate: [AuthGuard]
    }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {
    scrollPositionRestoration: "enabled"
});
