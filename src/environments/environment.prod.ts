export const environment = {
           urlApi: "http://localhost:8080",
           production: true,
           tokenWhitelistedDomains: [new RegExp("localhost:8080")],
           tokenBlacklistedRoutes: [new RegExp("/oauth/token")]
       };
